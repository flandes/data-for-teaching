# https://www.programcreek.com/python/example/101388/keras.datasets.cifar10.load_data
# https://www.programcreek.com/python/?project_name=abhishekrana%2FDeepFashion

import keras
import keras.datasets
from  keras.datasets import cifar10

def load_and_preprocess_data_3(num_classes=10):
    # The data, shuffled and split between train and test sets:
    (X_train, y_train), (x_test, y_test) = cifar10.load_data()

    # Convert class vectors to binary class matrices.
    y_train = keras.utils.to_categorical(y_train, num_classes)
    y_test = keras.utils.to_categorical(y_test, num_classes)

    X_train = X_train.astype('float32')
    x_test = x_test.astype('float32')
    X_train /= 255
    x_test /= 255

    N = X_train[0].shape
    input_shape = X_train.shape[1:]

    return X_train, x_test, y_train, y_test

X_train, x_test, y_train, y_test = load_and_preprocess_data_3()
