import numpy as np
data = np.load('~/fashion-mnist-reshaped.npz')
X = data['train_images']
y = data['train_labels']
X_test = data['test_images']
y_test = data['test_labels']
